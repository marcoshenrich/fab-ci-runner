#!/usr/bin/env bash
set -xeu

IMAGE=fab-ci-runner
CONTAINER=fab-ci-runner-container

if docker ps -a | grep "$CONTAINER" &>/dev/null ; then
    docker rm -f $CONTAINER
fi

docker run --privileged --name $CONTAINER -dit gitlab/dind:latest bash

docker cp . $CONTAINER:/home

# Install tup
docker exec $CONTAINER bash -c "apt-get update && \
    apt-get -y install ruby fuse && \
    apt-get -y install pkg-config libfuse-dev && \
    (cd /home/external/tup && ./bootstrap.sh && cp tup /usr/local/bin/tup)"

# Define the script we want run once the container boots
docker commit -c 'ENTRYPOINT ["wrapdocker"]' $CONTAINER $IMAGE

docker rm -f $CONTAINER

